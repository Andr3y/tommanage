package application;
	
import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.AnchorPane;
import javafx.fxml.FXMLLoader;


public class Start extends Application {
	private static Stage stage;
	@Override
	public void start(Stage primaryStage) {
		try {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("Menu.fxml"));
			AnchorPane root = loader.load();
			Scene MenuScene = new Scene(root);
			stage = primaryStage;
			MenuScene.getStylesheets().add("application.css");
			MenuController controller = loader.getController();
			controller.setStage(primaryStage);
			primaryStage.setScene(MenuScene);
			primaryStage.show();
			primaryStage.setResizable(false);
			primaryStage.setTitle("ToManage: Tomcat Performance Monitor");
			primaryStage.getIcons().add(new Image("images/logo.png"));
			primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>() {
			    @Override
			    public void handle(WindowEvent t) {
			        Platform.exit();
			        System.exit(0);
			    }
			});
		} catch(Exception e) {
			e.printStackTrace();
		}
	}

	public static Stage getPrimaryStage() {
	    return stage;
	}
	public static void start(String[] args) {
		launch(args);
	}
}
