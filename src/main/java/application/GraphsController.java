package application;

import java.io.IOException;
import java.net.URL;

import java.util.Optional;
import java.util.ResourceBundle;

import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServerConnection;
import javax.management.ObjectName;
import javax.management.ReflectionException;

import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;

import javafx.scene.control.Alert;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;

import javafx.scene.input.MouseEvent;
import javafx.stage.Stage;

import management.DataUpdater;

public class GraphsController implements Initializable{
	@FXML ListView<String> GraphList;
	
	@FXML CategoryAxis HeapAxis = new CategoryAxis();
    @FXML NumberAxis MBAxis = new NumberAxis();
    @FXML CategoryAxis TimeAxis = new CategoryAxis();
    @FXML NumberAxis ThreadAxis = new NumberAxis();	
    @FXML CategoryAxis TimeAxis2 = new CategoryAxis();
    @FXML NumberAxis CPUAxis = new NumberAxis();
    @FXML CategoryAxis TimeAxis3 = new CategoryAxis();
    @FXML NumberAxis ClassAxis = new NumberAxis();
    @FXML CategoryAxis TimeAxis4 = new CategoryAxis();
    @FXML NumberAxis RequestAxis = new NumberAxis();
    @FXML CategoryAxis TimeAxis5 = new CategoryAxis();
    @FXML NumberAxis MBAxisL = new NumberAxis();
    
    @FXML LineChart<String,Number> ThreadChart = new LineChart<String,Number>(TimeAxis,ThreadAxis);
    @FXML BarChart<String, Number> HeapChart = new BarChart<String,Number>(HeapAxis,MBAxis);
    @FXML LineChart<String,Number> CPUChart = new LineChart<String, Number>(TimeAxis2,CPUAxis);
    @FXML LineChart<String,Number> ClassChart = new LineChart<String, Number>(TimeAxis3,CPUAxis);
    @FXML LineChart<String,Number> RequestChart = new LineChart<String, Number>(TimeAxis4,RequestAxis);
    @FXML LineChart<String,Number> HeapChartL = new LineChart<String,Number>(TimeAxis5, MBAxisL);
	@FXML TextArea DescriptionArea;
	@FXML TextArea DescriptionArea2;
	
	CondensedGraphsController CController;
	DataUpdater updater;
	Scene CondensedScene;
	boolean first = true;
	GraphsController GController;
	
	MBeanServerConnection mbsc;
	ObjectName memoryName;
	ObjectName threadName;
	Stage window;
	boolean update = false;
	Alert error;
	Alert about;
	String timeFormat = "twenty seconds";
	boolean heapChartStyle = false;
	
	public void setCondensedScene(Scene CondensedScene) {
		this.CondensedScene = CondensedScene;
	}
	
	public void setStage(Stage window) {
		this.window = window;
	}
	
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		ThreadChart.setVisible(false);
		CPUChart.setVisible(false);
		ClassChart.setVisible(false);
		RequestChart.setVisible(false);
		HeapChartL.setVisible(false);
		DescriptionArea.setEditable(false);
		DescriptionArea2.setEditable(false);
		DescriptionArea2.setVisible(false);
	}
	
	public void initializeUI(MBeanServerConnection mbsc) {
		this.mbsc = mbsc;
		ObservableList<String> items = FXCollections.observableArrayList();
		try {
			updater = new DataUpdater(mbsc);
			updater.setGController(GController);
			items.add("Heap Memory Usage");
			items.add("Threading");
			items.add("Operating System");
			items.add("Classes");
			items.add("Requests");
			items.add("Miscellaneous Information");
			initializeCharts();
			updater.start();
			startUpdate();
			GraphList.setItems(items);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	private void initializeCharts(){
		error = new Alert(Alert.AlertType.ERROR);
		error.setTitle("Connection lost");
		error.setHeaderText("Connection to host has been lost");
		error.setContentText("Check the status of the server and try again");
		error.initOwner(window);
		
		about = new Alert(Alert.AlertType.INFORMATION);
		about.setTitle("About");
		about.setHeaderText("ToManage: Tomcat performance monitoring Java application");
		about.setContentText("Made by Andrej Gregic\nFaculty of Electrical Engineering and Computing (FER)\nFinal Assignment 2020");
		about.initOwner(window);

        HeapChart.setTitle("Heap Memory Usage");
        HeapAxis.setLabel("Info");       
        MBAxis.setLabel("MB");
	    HeapChart.setAnimated(false);
	    
	    ThreadChart.setAnimated(false);
        ThreadChart.setTitle("Live thread count");
        ThreadAxis.setLabel("Active threads");
        ThreadAxis.setAutoRanging(true);
        ThreadAxis.setTickUnit(1);
        TimeAxis.setLabel("Time");
        
        CPUChart.setAnimated(false);
        CPUChart.setTitle("System CPU load");
        CPUAxis.setLabel("CPU load");
        TimeAxis2.setLabel("Time");
        ClassChart.setAnimated(false);
        ClassChart.setTitle("Classes loaded");
        ClassAxis.setLabel("Classes");
        TimeAxis3.setLabel("Time");
        
        RequestChart.setAnimated(false);
        RequestChart.setTitle("Request processed");
        RequestAxis.setLabel("Request count");
        TimeAxis4.setLabel("Time");
        
        HeapChartL.setAnimated(false);
        HeapChartL.setTitle("Heap Memory Usage");
        MBAxisL.setLabel("MB");
        TimeAxis5.setLabel("Time");
	}
	
	public void startUpdate() {
		update = true;
		Thread updateThread = new Thread(() -> {
			while(update) {
				Platform.runLater(() -> {
				    	if(update) {
				    		try {
				    			String item = Optional.ofNullable(GraphList.getSelectionModel().getSelectedItem()).orElse("Heap Memory Usage");
								updateGraphs(item);
							} catch (AttributeNotFoundException | InstanceNotFoundException | MBeanException
									| ReflectionException | IOException e) {
								update = false;
								e.printStackTrace();
								error.showAndWait();
								switchSceneToMenu();
							}
				    	}					
				  });
				try {
					Thread.sleep(2100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		updateThread.start();	
	}
	
	private void updateGraphs(String item) throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
		updateThreadChart();
		updateCPUChart();
		updateHeapChart();
		updateHeapChartL();
		updateClassChart();
		updateRequestChart();
		if(item==null) return;
		if(item.equals("Heap Memory Usage")){
				DescriptionArea.setText(updater.getMemoryDescription());
			    ThreadChart.setVisible(false);
			    CPUChart.setVisible(false);
			    ClassChart.setVisible(false);
			    RequestChart.setVisible(false);
			    if(!heapChartStyle) {
			    	HeapChart.setVisible(true);
			    	HeapChartL.setVisible(false);
			    } else {
			    	HeapChartL.setVisible(true);
			    	HeapChart.setVisible(false);
			    }
		 } else if(item.equals("Threading")){
				DescriptionArea.setText(updater.getThreadDescription());
				CPUChart.setVisible(false);
			    HeapChart.setVisible(false);
			    HeapChartL.setVisible(false);
			    ClassChart.setVisible(false);
			    RequestChart.setVisible(false);
			    DescriptionArea2.setVisible(false);
			   	ThreadChart.setVisible(true);
		} else if(item.equals("Operating System")) {
				DescriptionArea.setText(updater.getOSDescription());
			   	ThreadChart.setVisible(false);
			    HeapChart.setVisible(false);
			    ClassChart.setVisible(false);
			    HeapChartL.setVisible(false);
			    RequestChart.setVisible(false);
			    DescriptionArea2.setVisible(false);
			    CPUChart.setVisible(true);
		} else if(item.equals("Classes")) {
				DescriptionArea.setText(updater.getClassDescription());
			   	ThreadChart.setVisible(false);
			    HeapChart.setVisible(false);
			    HeapChartL.setVisible(false);
			    CPUChart.setVisible(false);
			    RequestChart.setVisible(false);
			    DescriptionArea2.setVisible(false);
			    ClassChart.setVisible(true);
		} else if(item.equals("Requests")) {
				DescriptionArea.setText(updater.getRequestDescription());
			   	ThreadChart.setVisible(false);
			    HeapChart.setVisible(false);
			    HeapChartL.setVisible(false);
			    CPUChart.setVisible(false);
			    ClassChart.setVisible(false);
			    DescriptionArea2.setVisible(false);
			    RequestChart.setVisible(true);
		} else if(item.equals("Miscellaneous Information")) {
				DescriptionArea2.setText(updater.getRuntimeDescription());
				DescriptionArea.setText(updater.getCompilationDescription());
			   	ThreadChart.setVisible(false);
			    HeapChart.setVisible(false);
			    HeapChartL.setVisible(false);
			    CPUChart.setVisible(false);
			    ClassChart.setVisible(false);
			    RequestChart.setVisible(false);
			    DescriptionArea2.setVisible(true);
		}
	}

	private void updateCPUChart() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
			CPUChart.getData().clear();
			CPUChart.getData().add(updater.getCPUSeries(timeFormat));
	}

	private void updateThreadChart() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException{
			ThreadChart.getData().clear();
			ThreadChart.getData().add(updater.getThreadSeries(timeFormat));
	}

	private void updateHeapChart() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {    
		   	HeapChart.getData().clear();
		   	HeapChart.getData().add(updater.getHeapSeries());
	}
	
	private void updateHeapChartL() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {    
		   	HeapChartL.getData().clear();
		   	HeapChartL.getData().add(updater.getHeapSeriesL(timeFormat));
}
	
	private void updateClassChart() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {    
			ClassChart.getData().clear();
	   		ClassChart.getData().add(updater.getClassSeries(timeFormat));
	}
	
	private void updateRequestChart() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {    
		RequestChart.getData().clear();
   		RequestChart.getData().add(updater.getRequestSeries(timeFormat));
	}	
	
	@FXML public void handleListMouseClick(MouseEvent arg0) throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
	    String item = GraphList.getSelectionModel().getSelectedItem();
	    updateGraphs(item);
	}
	

	@FXML public void handleNewConnectionMouseClick(ActionEvent arg0) throws IOException {
	    switchSceneToMenu();
	}
	
	@FXML public void switchToCondensedView() {
		update = false;
		if(first) {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("CondensedGraphs.fxml"));
			Parent CondensedParent;
			try {
				CondensedParent = loader.load();
				CondensedScene = new Scene(CondensedParent);
				CondensedScene.getStylesheets().add("chart.css");
		        CController = loader.getController();
		        updater.setCController(CController);
		        CController.initializeUI(updater, heapChartStyle);
		        CController.setStage(window);
		        CController.setListScene(GraphList.getScene());
		        CController.setGraphController(GController);

		        first = false;
			} catch (IOException e) {
				e.printStackTrace();
			} 
		}
        CController.setTimeFormat(timeFormat);
        if(!timeFormat.equals("twenty seconds")) CController.setTickVisible(false);
        CController.setHeapChartStyle(heapChartStyle);
		CController.startUpdate();
        window.setScene(CondensedScene);
        window.show();
	}
	
	public void switchSceneToMenu(){
		update = false;
		updater.stop();
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("Menu.fxml"));
		Parent MenuParent;
		try {
			MenuParent = loader.load();
			Scene MenuScene = new Scene(MenuParent);
			window.setScene(MenuScene);
		    window.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	public void setController(GraphsController controller) {
		this.GController = controller;
		
	}
	
	public void setTimeFormatToTwentySeconds () throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
		timeFormat = "twenty seconds";
		updateHeapChartL();
		updateThreadChart();
		updateCPUChart();
		updateRequestChart();
		updateClassChart();
		setTickVisible(true);
		if(CController != null ) CController.setTickVisible(true);
	}
	
	public void setTimeFormatToMinute() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
		timeFormat = "minute";
		updateHeapChartL();
		updateThreadChart();
		updateCPUChart();
		updateRequestChart();
		updateClassChart();
		setTickVisible(false);
		if(CController != null ) CController.setTickVisible(false);
	}
	
	public void setTimeFormatToFiveMinutes() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
		timeFormat = "five minutes";
		updateHeapChartL();
		updateThreadChart();
		updateCPUChart();
		updateRequestChart();
		updateClassChart();
		setTickVisible(false);
		if(CController != null ) CController.setTickVisible(false);
	}
	
	public void setTimeFormatToTenMinutes() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
		timeFormat = "ten minutes";
		updateHeapChartL();
		updateThreadChart();
		updateCPUChart();
		updateRequestChart();
		updateClassChart();
		setTickVisible(false);
		if(CController != null ) CController.setTickVisible(false);
	}
	public void setTimeFormatToThirtyMinutes() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
		timeFormat = "thirty minutes";
		updateHeapChartL();
		updateThreadChart();
		updateCPUChart();
		updateRequestChart();
		updateClassChart();
		setTickVisible(false);
		if(CController != null ) CController.setTickVisible(false);
	}
	public void setTimeFormatToHour() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
		timeFormat = "hour";
		updateHeapChartL();
		updateThreadChart();
		updateCPUChart();
		updateRequestChart();
		updateClassChart();
		setTickVisible(false);
		if(CController != null ) CController.setTickVisible(false);
	}
	
	public void gc() {
		updater.gc();
	}
	
	public void resetPeakThreadCount() {
		updater.resetPeakThreadCount();
	}
	
	public void findDeadlock() {
		updater.findDeadlock();
	}
	
	public void findMonitorDeadlock() {
		updater.findMonitorDeadlock();
	}
	
	public void setTickVisible(boolean bool) {
	   ThreadChart.setCreateSymbols(bool);
       CPUChart.setCreateSymbols(bool);
       ClassChart.setCreateSymbols(bool);
       RequestChart.setCreateSymbols(bool);
       HeapChartL.setCreateSymbols(bool);
	}

	public void setTimeFormat(String timeFormat) throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
		this.timeFormat = timeFormat;
		updateThreadChart();
		updateCPUChart();
		updateClassChart();
	}
	
	public void changeHeapChartStyle() {
		heapChartStyle = !heapChartStyle;
		HeapChart.setVisible(!HeapChart.isVisible());
		HeapChartL.setVisible(!HeapChartL.isVisible());
	}
	
	public void setHeapChartStyle(boolean style) {
		heapChartStyle = style;
		HeapChart.setVisible(!HeapChart.isVisible());
		HeapChartL.setVisible(!HeapChartL.isVisible());
	}
	
	public void displayAbout() {
		about.showAndWait();
	}
	
	public Stage getWindow() {
		return window;
		}
	}
