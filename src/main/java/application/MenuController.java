package application;

import java.io.IOException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import javax.management.MBeanServerConnection;
import javax.management.remote.JMXConnector;
import javax.management.remote.JMXConnectorFactory;
import javax.management.remote.JMXServiceURL;

import javafx.beans.binding.Bindings;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.stage.Stage;

public class MenuController implements Initializable {
	@FXML TextField HostField;
	@FXML TextField PortField;
	@FXML Button ConnectButton;
	@FXML Label NumberLabel;
	@FXML AnchorPane ConnectPane;
	Alert error;

	Pattern PortPattern = Pattern.compile("^([0-9]+)$"); 
	Scene scene;
	Stage window;
	MBeanServerConnection mbsc;
	boolean first = true;
	
		public void setStage(Stage window) {
			this.window = window;
		}
	
		public void onConnectClick() throws NumberFormatException, IOException {
			connect(false, "", "");
		}
		
		public void connect(boolean authentication, String username, String password) {
			JMXConnector jmxc;
			try {
				String surl = "service:jmx:rmi:///jndi/rmi://" + HostField.getText() + ":" + Integer.parseInt(PortField.getText()) + "/jmxrmi";
				JMXServiceURL url = new JMXServiceURL(surl);
				if(authentication) {
					Map<String, String[]> creds= new HashMap<>();
					String[] credentials = {username, password};
					creds.put(JMXConnector.CREDENTIALS, credentials);   
					jmxc = JMXConnectorFactory.connect(url, creds);
				}
				else jmxc = JMXConnectorFactory.connect(url);
				mbsc = jmxc.getMBeanServerConnection();
				switchScene();
			}catch (SecurityException se) {
				if(authentication==false) getCredentials();
				else AlertBox.display("Authorization failed", "Security check failed. Recheck your username and password");
			} catch(Exception ioe) {
				ioe.printStackTrace();
				error.showAndWait();
			}
		}
		private void getCredentials() {
			Stage myDialog = new CredentialsDialog(window);
	        myDialog.showAndWait();
		}

		private void switchScene() throws IOException {
			FXMLLoader loader = new FXMLLoader();
			loader.setLocation(getClass().getResource("Graphs.fxml"));
			Parent graphsParent = loader.load();
	        Scene graphsScene = new Scene(graphsParent);
	        graphsScene.getStylesheets().add("chart.css");
	        window = Start.getPrimaryStage();
	        GraphsController controller = loader.getController();
	        controller.setController(controller);
	        controller.setStage(window);
	        controller.initializeUI(mbsc);
	        window.setScene(graphsScene);
	        window.show();
		}
		@Override
		public void initialize(URL arg0, ResourceBundle arg1) {
			ConnectButton.disableProperty().bind(Bindings.createBooleanBinding( () -> !(!HostField.getText().isEmpty() && CheckPort()) , HostField.textProperty(), PortField.textProperty()));
			NumberLabel.setVisible(false);
			error = new Alert(Alert.AlertType.ERROR);
			error.setTitle("Connection unavailable");
			error.setHeaderText("Host could not be reached");
			error.setContentText("Please check the host and port information");
			error.initOwner(Start.getPrimaryStage());
		}
		
		@FXML public void keyPressed(KeyEvent keyEvent) throws NumberFormatException, IOException {
			KeyCode code = keyEvent.getCode();
		    if (code == KeyCode.TAB) {
		        if(HostField.isFocused()) PortField.requestFocus();
		        if(PortField.isFocused()) HostField.requestFocus();
		    } else if(code == KeyCode.ENTER && !ConnectButton.isDisabled()) {
		    	onConnectClick();
		    }
		}
		private boolean CheckPort() {
			String text = PortField.getText();
			if(text.isEmpty()) return false;
			Matcher matcher = PortPattern.matcher(text);
			if(matcher.matches()) {
				NumberLabel.setVisible(false);
				return true;
			}
			else {
				NumberLabel.setVisible(true);
				return false;
			}
		}
		class CredentialsDialog extends Stage {

			 public CredentialsDialog(Stage owner) {
			     super();
			     initOwner(owner);
			     setTitle("Enter credentials");
			     Group root = new Group();
			     Scene scene = new Scene(root, 265, 110, Color.WHITE);
			     setScene(scene);

			     GridPane gridpane = new GridPane();
			     gridpane.setPadding(new Insets(5));
			     gridpane.setHgap(5);
			     gridpane.setVgap(5);

			     Label userNameLbl = new Label("Username: ");
			     gridpane.add(userNameLbl, 0, 1);

			     Label passwordLbl = new Label("Password: ");
			     gridpane.add(passwordLbl, 0, 2);
			     final TextField userNameField = new TextField();
			     gridpane.add(userNameField, 1, 1);

			     final PasswordField passwordField = new PasswordField();
			     gridpane.add(passwordField, 1, 2);

			     Button login = new Button("Connect");
			     login.setOnAction(new EventHandler<ActionEvent>() {
			         public void handle(ActionEvent event) {
			             connect(true, userNameField.getText().toString(), passwordField.getText().toString());
			             close();
			         }
			     });
			     gridpane.add(login, 1, 3);
			     GridPane.setHalignment(login, HPos.CENTER);
			     root.getChildren().add(gridpane);
			 		}
				}
}
