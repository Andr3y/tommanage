package application;

import java.io.IOException;
import java.net.URL;

import java.util.ResourceBundle;

import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.ReflectionException;
import javafx.application.Platform;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.chart.BarChart;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.control.Alert;
import javafx.stage.Stage;
import management.DataUpdater;

public class CondensedGraphsController implements Initializable{
	@FXML CategoryAxis HeapAxis = new CategoryAxis();
    @FXML NumberAxis MBAxis = new NumberAxis();
    @FXML CategoryAxis TimeAxis = new CategoryAxis();
    @FXML NumberAxis ThreadAxis = new NumberAxis();	
    @FXML CategoryAxis TimeAxis2 = new CategoryAxis();
    @FXML NumberAxis CPUAxis = new NumberAxis();	
    @FXML CategoryAxis TimeAxis3 = new CategoryAxis();
    @FXML NumberAxis ClassAxis = new NumberAxis();	
    @FXML CategoryAxis TimeAxis4 = new CategoryAxis();
    @FXML NumberAxis MBAxisL = new NumberAxis();
    

    @FXML BarChart<String, Number> HeapChartC = new BarChart<String,Number>(HeapAxis,MBAxis);
    @FXML LineChart<String,Number> ThreadChartC = new LineChart<String,Number>(TimeAxis,ThreadAxis);
    @FXML LineChart<String,Number> CPUChartC = new LineChart<String, Number>(TimeAxis2,CPUAxis);
    @FXML LineChart<String,Number> ClassChartC = new LineChart<String, Number>(TimeAxis3,ClassAxis);
    @FXML LineChart<String,Number> HeapChartL = new LineChart<String,Number>(TimeAxis4, MBAxisL);
    
	DataUpdater updater;
	
	Stage window;
	Scene ListScene;
	boolean first = true;
	boolean update = false;
	Alert error;
	Alert about;
	GraphsController GController;
	String timeFormat = "twenty seconds";
	boolean heapChartStyle = false;
	
	public void setListScene(Scene ListScene) {
		this.ListScene = ListScene;
	}
	
	public void setStage(Stage window) {
		this.window = window;
	}
	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
	}
	
	public void initializeUI(DataUpdater updater, boolean heapChartStyle) {
		this.updater = updater;
		this.heapChartStyle = heapChartStyle;
		if(!heapChartStyle) HeapChartL.setVisible(false);
		else HeapChartC.setVisible(false);
		initializeCharts();
	}
	
	private void initializeCharts(){
		error = new Alert(Alert.AlertType.ERROR);
		error.setTitle("Connection lost");
		error.setHeaderText("Connection to host has been lost");
		error.setContentText("Check the status of the server and try again");

		about = new Alert(Alert.AlertType.INFORMATION);
		about.setTitle("About");
		about.setHeaderText("ToManage: Tomcat performance monitoring Java application");
		about.setContentText("Made by Andrej Gregic\nFaculty of Electrical Engineering and Computing (FER)\nFinal Assignment 2020");
		about.initOwner(window);
		
        HeapChartC.setTitle("Heap Memory Usage");
        HeapAxis.setLabel("Info");       
        MBAxis.setLabel("MB");
	    HeapChartC.setAnimated(false);
	    
	    ThreadChartC.setAnimated(false);
        ThreadChartC.setTitle("Live thread count");
        ThreadAxis.setLabel("Active threads");
        ThreadAxis.setAutoRanging(false);
        ThreadAxis.setLowerBound(20);
        ThreadAxis.setTickUnit(1);
        TimeAxis.setLabel("Time");
        
        CPUChartC.setAnimated(false);
        CPUChartC.setTitle("System CPU load");
        CPUAxis.setLabel("CPU load");
        TimeAxis2.setLabel("Time");
        
        ClassChartC.setAnimated(false);
        ClassChartC.setTitle("Classes loaded");
        ClassAxis.setLabel("Classes");
        TimeAxis3.setLabel("Time");
        
        HeapChartL.setAnimated(false);
        HeapChartL.setTitle("Heap Memory Usage");
        MBAxisL.setLabel("MB");
        TimeAxis4.setLabel("Time");   
	}
	
		private void updateGraphs() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
			if(!heapChartStyle) {
				HeapChartL.setVisible(false);
				HeapChartC.setVisible(true);
			}
			else {
				HeapChartC.setVisible(false);
				HeapChartL.setVisible(true);
			}
			updateThreadChart();
			updateHeapChart();
			updateCPUChart();
			updateClassChart();
			updateHeapChartL();
		}
	
		public void updateThreadChart() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException{
			ThreadChartC.getData().clear();
			ThreadChartC.getData().add(updater.getThreadSeries(timeFormat));
		}
		
		private void updateCPUChart() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
			CPUChartC.getData().clear();
			CPUChartC.getData().add(updater.getCPUSeries(timeFormat));
		}

		private void updateHeapChart() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {    
			  HeapChartC.getData().clear();
			  HeapChartC.getData().add(updater.getHeapSeries());
		}
		
		private void updateHeapChartL() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {    
			  HeapChartL.getData().clear();
			  HeapChartL.getData().add(updater.getHeapSeriesL(timeFormat));
		}
		
		private void updateClassChart() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {    
			  ClassChartC.getData().clear();
			  ClassChartC.getData().add(updater.getClassSeries(timeFormat));
		}

	@FXML public void handleNewConnectionMouseClick(ActionEvent arg0) throws IOException {
	    switchSceneToMenu();
	}
	
	@FXML public void switchToListView() throws IOException {
		update = false;
		GController.startUpdate();
		try {
			GController.setTimeFormat(timeFormat);
			GController.setHeapChartStyle(heapChartStyle);
		} catch (AttributeNotFoundException | InstanceNotFoundException | MBeanException | ReflectionException
				| IOException e) {
			e.printStackTrace();
		}
		window.setScene(ListScene);
        window.show();
	}
	
	private void switchSceneToMenu(){
		update = false;
		updater.stop();
		FXMLLoader loader = new FXMLLoader();
		loader.setLocation(getClass().getResource("Menu.fxml"));
		Parent MenuParent;
		try {
			MenuParent = loader.load();
			Scene MenuScene = new Scene(MenuParent);
			window.setScene(MenuScene);
		    window.show();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}

	public void startUpdate() {
		update = true;
		Thread updateThread = new Thread(() -> {
			while(update) {
				Platform.runLater(() -> {
				    	if(update) {
				    		try {
								updateGraphs();
							} catch (AttributeNotFoundException | InstanceNotFoundException | MBeanException
									| ReflectionException | IOException e) {
								update = false;
								e.printStackTrace();
								error.showAndWait();
								switchSceneToMenu();
							}
				    	}					
				  });
				try {
					Thread.sleep(2100);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		});
		updateThread.start();	
	}

	public void setGraphController(GraphsController GController) {
		this.GController = GController;	
	}

	public void stopUpdate() {
		update = false;
	}
	
	public void setTimeFormatToTwentySeconds () throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
		timeFormat = "twenty seconds";
		updateHeapChartL();
		updateThreadChart();
		updateCPUChart();
		setTickVisible(true);
	    GController.setTickVisible(true);
	}
	
	public void setTimeFormatToMinute() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
		timeFormat = "minute";
		updateHeapChartL();
		updateThreadChart();
		updateCPUChart();
		updateClassChart();
		setTickVisible(false);
		GController.setTickVisible(false);
	}
	
	public void setTimeFormatToFiveMinutes() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
		timeFormat = "five minutes";
		updateHeapChartL();
		updateThreadChart();
		updateCPUChart();
		updateClassChart();
		setTickVisible(false);
		GController.setTickVisible(false);
	}
	
	public void setTimeFormatToTenMinutes() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
		timeFormat = "ten minutes";
		updateHeapChartL();
		updateThreadChart();
		updateCPUChart();
		updateClassChart();
		setTickVisible(false);
		GController.setTickVisible(false);
	}
	public void setTimeFormatToThirtyMinutes() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
		timeFormat = "thirty minutes";
		updateHeapChartL();
		updateThreadChart();
		updateCPUChart();
		updateClassChart();
		setTickVisible(false);
		GController.setTickVisible(false);
	}
	public void setTimeFormatToHour() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
		timeFormat = "hour";
		updateHeapChartL();
		updateThreadChart();
		updateCPUChart();
		updateClassChart();
		setTickVisible(false);
		GController.setTickVisible(false);
	}
	
	public void gc() {
		updater.gc();
	}
	
	public void resetPeakThreadCount() {
		updater.resetPeakThreadCount();
	}
	
	public void findDeadlock() {
		updater.findDeadlock();
	}
	
	public void findMonitorDeadlock() {
		updater.findMonitorDeadlock();
	}
	
	public void setTickVisible(boolean bool) {
		   ThreadChartC.setCreateSymbols(bool);
	       CPUChartC.setCreateSymbols(bool);
	       ClassChartC.setCreateSymbols(bool);
	       HeapChartL.setCreateSymbols(bool);
		}

	public void setTimeFormat(String timeFormat) {
		this.timeFormat = timeFormat;
	}
	
	public void changeHeapChartStyle() {
		heapChartStyle = !heapChartStyle;
		HeapChartC.setVisible(!HeapChartC.isVisible());
		HeapChartL.setVisible(!HeapChartL.isVisible());
	}
	
	public void setHeapChartStyle(boolean style) {
		heapChartStyle = style;
		HeapChartC.setVisible(!HeapChartC.isVisible());
		HeapChartL.setVisible(!HeapChartL.isVisible());
	}
	
	public void displayAbout() {
		about.showAndWait();
	}
}
