package management;

import java.io.IOException;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.ThreadMXBean;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Iterator;
import java.util.Set;

import javax.management.AttributeNotFoundException;
import javax.management.InstanceNotFoundException;
import javax.management.MBeanException;
import javax.management.MBeanServerConnection;
import javax.management.MalformedObjectNameException;
import javax.management.ObjectName;
import javax.management.ReflectionException;
import javax.management.openmbean.CompositeDataSupport;

import application.AlertBox;
import application.CondensedGraphsController;
import application.GraphsController;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Series;
import javafx.scene.control.Alert;
import javafx.stage.Stage;

public class DataUpdater {
		StringBuilder MemoryDescription = new StringBuilder();
		StringBuilder ThreadDescription = new StringBuilder();
		StringBuilder OSDescription = new StringBuilder();
		StringBuilder ClassDescription = new StringBuilder();
		StringBuilder RequestDescription = new StringBuilder();
		StringBuilder RuntimeDescription = new StringBuilder();
		StringBuilder CompilationDescription = new StringBuilder();
		
		private static DecimalFormat df = new DecimalFormat("0.0");
		final SimpleDateFormat SecondsFormat = new SimpleDateFormat("HH:mm:ss");
		final SimpleDateFormat MinuteFormat = new SimpleDateFormat("HH:mm");
		final SimpleDateFormat HourFormat = new SimpleDateFormat("HH");
		
		ObjectName memoryName;
		ObjectName threadName;
		ObjectName operatingSystemName;
		ObjectName className;
		ObjectName requestName;
		ObjectName runtimeName;
		ObjectName compilationName;
	    MemoryMXBean memoryProxy;
	    ThreadMXBean threadProxy;
		MBeanServerConnection mbsc;
		boolean update = true;
		CondensedGraphsController CController;
		GraphsController GController;
		Alert error; 
		Alert gcAlert;
		Stage window;
		
	    ObservableList<XYChart.Data<String, Number>> HeapData = FXCollections.observableArrayList();
	    ObservableList<XYChart.Data<Calendar, Number>> HeapDataL = FXCollections.observableArrayList();
	    ObservableList<XYChart.Data<Calendar, Number>> ThreadData = FXCollections.observableArrayList();
	    ObservableList<XYChart.Data<Calendar, Number>> CPUData = FXCollections.observableArrayList();
	    ObservableList<XYChart.Data<Calendar, Number>> ClassData = FXCollections.observableArrayList();
	    ObservableList<XYChart.Data<Calendar, Number>> RequestData = FXCollections.observableArrayList();
	    
	    double usedH;
	    int threadCount;
	    int peakThreadCount;
	    double systemCPULoad; 
	    int LoadedClassCount;
	    int requestCount;
	    
		public DataUpdater(MBeanServerConnection mbsc) throws IOException {
			this.mbsc = mbsc;
			 try {
				 	Set<ObjectName> names = mbsc.queryNames(new ObjectName("Catalina:type=GlobalRequestProcessor,*"), null);
				 	Iterator<ObjectName> iter = names.iterator();
				 	ObjectName name = iter.next();
				 	String parse[] = name.toString().split("\"");
					memoryName = new ObjectName("java.lang:type=Memory");
					threadName = new ObjectName("java.lang:type=Threading");
					operatingSystemName = new ObjectName("java.lang:type=OperatingSystem");
					className = new ObjectName("java.lang:type=ClassLoading");
					runtimeName = new ObjectName("java.lang:type=Runtime");
					compilationName = new ObjectName("java.lang:type=Compilation");
					requestName = new ObjectName("Catalina:type=GlobalRequestProcessor,name=" + "\"" + parse[1] + "\"");
					memoryProxy = ManagementFactory.newPlatformMXBeanProxy(mbsc, ManagementFactory.MEMORY_MXBEAN_NAME, MemoryMXBean.class);
					threadProxy = ManagementFactory.newPlatformMXBeanProxy(mbsc, ManagementFactory.THREAD_MXBEAN_NAME, ThreadMXBean.class);
				} catch (MalformedObjectNameException e) {
					e.printStackTrace();
				}
			 	error = new Alert(Alert.AlertType.ERROR);
				error.setTitle("Connection lost");
				error.setHeaderText("Connection to host has been lost");
				error.setContentText("Check the status of the server and try again");
				
				gcAlert = new Alert(Alert.AlertType.INFORMATION);
				gcAlert.setTitle("Garbage collection");
				gcAlert.setHeaderText("Garbage collection successful!");
				gcAlert.setContentText("Garbage collection memory operation has been successfully called!");

		}
				
		
		public void updateThreadInfo() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException{
			threadCount = (int) mbsc.getAttribute(threadName, "ThreadCount");
			peakThreadCount = (int) mbsc.getAttribute(threadName, "PeakThreadCount");
			long startedThreadCount = (long) mbsc.getAttribute(threadName, "TotalStartedThreadCount");
			int daemonThreadCount = (int) mbsc.getAttribute(threadName, "DaemonThreadCount");
			Calendar time= Calendar.getInstance();
		    ThreadData.add(new XYChart.Data<>(time, threadCount)); 
		    
		    
			 ThreadDescription.setLength(0);
			 ThreadDescription.append("Current thread count: " + threadCount + "\n");
			 ThreadDescription.append("Peak thread count: " + peakThreadCount + "\n");
			 ThreadDescription.append("Daemon thread count: " + daemonThreadCount + "\n");
			 ThreadDescription.append("Total started thread count: " + startedThreadCount + "\n");
			 ThreadDescription.append("Last update: " + SecondsFormat.format(time.getTime()));
		}
		
		public String getThreadDescription() {
			return ThreadDescription.toString();
		}
		
		public void updateHeapInfo() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException{
			
			CompositeDataSupport heapMemoryUsage = (CompositeDataSupport) mbsc.getAttribute(memoryName, "HeapMemoryUsage" );
		    CompositeDataSupport nonHeapMemoryUsage = (CompositeDataSupport) mbsc.getAttribute(memoryName, "NonHeapMemoryUsage" );
		   

			usedH = (double) ((Long) heapMemoryUsage.get("used"))/(1000000);
		    Double committedH =  (double) ((Long) heapMemoryUsage.get("committed"))/(1000000);
		    Double initH = (double) ((Long) heapMemoryUsage.get("init"))/(1000000);
		    Double maxH = (double) ((Long) heapMemoryUsage.get("max"))/(1000000);
		    
		    Double usedN = (double) ((Long) nonHeapMemoryUsage.get("used"))/(1000000);
		    Double initN = (double) ((Long) nonHeapMemoryUsage.get("init"))/(1000000);
		    Double committedN = (double) ((Long) nonHeapMemoryUsage.get("committed"))/(1000000);
			
		    Calendar time = Calendar.getInstance();
		    HeapDataL.add(new XYChart.Data<>(time, usedH));
		    
			 MemoryDescription.setLength(0);
			 MemoryDescription.append("Currently used heap memory: " + df.format(usedH) + "MB\n");
			 MemoryDescription.append("Committed memory size: " + df.format(committedH) + "MB\n");
			 MemoryDescription.append("Initial memory size: " + df.format(initH) + "MB\n");
			 MemoryDescription.append("Maximum memory size: " + df.format(maxH) + "MB\n");
			 
			 MemoryDescription.append("\nNon-heap memory usage:\n");
			 MemoryDescription.append("Currently used: " + df.format(usedN) + "MB\n");
			 MemoryDescription.append("Committed: " + df.format(committedN) + "MB\n");
			 MemoryDescription.append("Initial: " + df.format(initN) + "MB\n");
			 MemoryDescription.append("Last update: " + SecondsFormat.format(time.getTime()));
			
			 HeapData.clear();
			 HeapData.add(new XYChart.Data<String, Number>("Used: "+ df.format(usedH) + "MB", usedH ));
			 HeapData.add(new XYChart.Data<String, Number>("Committed: " + df.format(committedH)+ "MB",committedH));
			 HeapData.add(new XYChart.Data<String, Number>("Init: " + df.format(initH)+ "MB", initH ));
			 HeapData.add(new XYChart.Data<String, Number>("Max: " + df.format(maxH)+ "MB", maxH));
			 
		}
		
		public String getMemoryDescription() {
			return MemoryDescription.toString();
		}
		
		public void updateOSInfo() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
			String architecture = (String) mbsc.getAttribute(operatingSystemName, "Arch" );
		    int availableProcessors = (int) mbsc.getAttribute(operatingSystemName, "AvailableProcessors" );
			systemCPULoad =  ((double) mbsc.getAttribute(operatingSystemName, "SystemCpuLoad"))*100; //%
			Double processCPULoad =  ((double) mbsc.getAttribute(operatingSystemName, "ProcessCpuLoad"))*100; //%
			long totalMemorySize = (long) mbsc.getAttribute(operatingSystemName, "TotalMemorySize");
			long freeMemorySize = (long) mbsc.getAttribute(operatingSystemName, "FreeMemorySize");
			long totalPhysicalMemorySize = (long) mbsc.getAttribute(operatingSystemName, "TotalPhysicalMemorySize");
			long freePhysicalMemorySize = (long) mbsc.getAttribute(operatingSystemName, "FreePhysicalMemorySize");
			long totalSwapSpaceSize = (long) mbsc.getAttribute(operatingSystemName, "TotalSwapSpaceSize");
			long freeSwapSpaceSize = (long) mbsc.getAttribute(operatingSystemName, "FreeSwapSpaceSize");
	
			Calendar time= Calendar.getInstance();
		    CPUData.add(new XYChart.Data<>(time, systemCPULoad));
		      
			 OSDescription.setLength(0);
			 OSDescription.append("System CPU load: " + df.format(systemCPULoad) + "%\n");
			 OSDescription.append("Process CPU load (JVM): " + df.format(processCPULoad) + "%\n");
			 OSDescription.append("Architecture: " + architecture + "\n");
			 OSDescription.append("Available processors: " + availableProcessors + "\n");
			 OSDescription.append("Total Memory Size: " + totalMemorySize/1000000  + " MB\n");
			 OSDescription.append("Free Memory Size: " + freeMemorySize/1000000 + " MB\n");
			 OSDescription.append("Total Physical Memory Size: " + totalPhysicalMemorySize/1000000 + " MB\n");
			 OSDescription.append("Free Physical Memory Size: " + freePhysicalMemorySize/1000000 + " MB\n");
			 OSDescription.append("Total Swap Space Size: " + totalSwapSpaceSize/1000000 + " MB\n");
			 OSDescription.append("Free Swap Space Size: " + freeSwapSpaceSize/1000000 + " MB\n");
			 OSDescription.append("Last update: " + SecondsFormat.format(time.getTime()));
		}
	
		public String getOSDescription() {
			return OSDescription.toString();
		}
		
		public void updateClassInfo() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
			LoadedClassCount= (int) mbsc.getAttribute(className, "LoadedClassCount");
			long TotalLoadedClassCount= (long) mbsc.getAttribute(className, "TotalLoadedClassCount");
			long UnloadedClassCount= (long) mbsc.getAttribute(className, "UnloadedClassCount");
			
			Calendar time= Calendar.getInstance();
		    ClassData.add(new XYChart.Data<>(time, LoadedClassCount));
		      
			ClassDescription.setLength(0);
			ClassDescription.append("Loaded classes count: " + LoadedClassCount + "\n");
			ClassDescription.append("Total loaded classes count: " + TotalLoadedClassCount + "\n");
			ClassDescription.append("Unloaded classes count: " + UnloadedClassCount + "\n");
			ClassDescription.append("Last update: " + SecondsFormat.format(time.getTime()));
		}
		
		public String getClassDescription() {
			return ClassDescription.toString();
		}
		
		public void updateRequestInfo() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
			requestCount = (int) mbsc.getAttribute(requestName, "requestCount");
			long maxTime = (long) mbsc.getAttribute(requestName, "maxTime");
			long processingTime = (long) mbsc.getAttribute(requestName, "processingTime");
			int errorCount = (int) mbsc.getAttribute(requestName, "errorCount");
			
			Calendar time= Calendar.getInstance();
		    RequestData.add(new XYChart.Data<>(time, requestCount));
		      
			RequestDescription.setLength(0);
			RequestDescription.append("Number of request processed: " + requestCount + "\n");
			RequestDescription.append("Maximum time to process a request: " + maxTime + "ms\n");
			RequestDescription.append("Total time to process the requests: " + processingTime + "ms\n");
			RequestDescription.append("Number of errors: " + errorCount + "\n");
			RequestDescription.append("Last update: " + SecondsFormat.format(time.getTime()));
		}
		
		public String getRequestDescription() {
			return RequestDescription.toString();
		}
		
		public void updateRuntimeInfo() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
				String classPath = (String) mbsc.getAttribute(runtimeName, "ClassPath");
				String libraryPath = (String) mbsc.getAttribute(runtimeName, "LibraryPath");
				String vmName = (String) mbsc.getAttribute(runtimeName, "VmName");
				String vmVendor = (String) mbsc.getAttribute(runtimeName, "VmVendor");
				String vmVersion = (String) mbsc.getAttribute(runtimeName, "VmVersion");
				long uptime = (long) mbsc.getAttribute(runtimeName, "Uptime");
				long startTime = (long) mbsc.getAttribute(runtimeName, "StartTime");
				
				String classPathParts[] = classPath.split(";");
				String libraryPathParts[] = libraryPath.split(";");
				
				RuntimeDescription.setLength(0);
				RuntimeDescription.append("Uptime: " + uptime/1000 + "s\n");
				RuntimeDescription.append("Start Time: " + startTime/1000 + "s\n");
				RuntimeDescription.append("Virtual Machine Name : " + vmName + "\n");
				RuntimeDescription.append("Virtual Machine Vendor: " + vmVendor + "\n");
				RuntimeDescription.append("Virtual Machine Version: " + vmVersion + "\n");
				
				RuntimeDescription.append("Class Path: " + classPathParts[0] + "\n");
				for(int i =1; i<classPathParts.length;i++) {
					RuntimeDescription.append(classPathParts[i] + "\n");
				}
				RuntimeDescription.append("\n");
				RuntimeDescription.append("Library Path: " + libraryPathParts[0] + "\n");
				for(int i =1; i<libraryPathParts.length;i++) {
					RuntimeDescription.append(libraryPathParts[i] + "\n");
				}
				RuntimeDescription.append("\n");	
				
		}
		
		public String getRuntimeDescription() {
			return RuntimeDescription.toString();
		}
		
		public void updateCompilationInfo() throws AttributeNotFoundException, InstanceNotFoundException, MBeanException, ReflectionException, IOException {
			long totalCompilationTime = (long) mbsc.getAttribute(compilationName, "TotalCompilationTime");
			String compilerName = (String) mbsc.getAttribute(compilationName, "Name");
			
			CompilationDescription.setLength(0);
			CompilationDescription.append("Total Compilation Time: " + totalCompilationTime + "ms\n");
			CompilationDescription.append("Compiler Name: " + compilerName + "\n");

	}
	
		public String getCompilationDescription() {
			return CompilationDescription.toString();
		}
		
		public Series<String, Number> getHeapSeries() {
			ObservableList<XYChart.Data<String,Number>> chartData = FXCollections.observableArrayList();
			for (int i = 0; i < HeapData.size(); i++) {
	            XYChart.Data<String,Number> d =  HeapData.get(i);
	            chartData.add(d);
			}
			return new Series<String, Number>("Heap Memory Usage: " + df.format(usedH) + " MB", chartData);
		}


		public Series<String, Number> getHeapSeriesL(String timeFormat) {
			ObservableList<XYChart.Data<String,Number>> chartData = FXCollections.observableArrayList();
			switch(timeFormat) {
			case "twenty seconds":
				Formatter.formatDataToTwentySeconds(chartData, HeapDataL);
				break;
			case "minute":
				Formatter.formatDataToMinute(chartData, HeapDataL);
				break;
			case "five minutes":
				Formatter.formatDataToFiveMinutes (chartData, HeapDataL);
				break;
			case "ten minutes":
				Formatter.formatDataToTenMinutes(chartData, HeapDataL);
				break;
			case "thirty minutes":
				Formatter.formatDataToThirtyMinutes(chartData, HeapDataL);
				break;
			case "hour":
				Formatter.formatDataToHour(chartData, HeapDataL);
				break;
			}
			return new Series<String, Number>("Heap Memory Usage: " + df.format(usedH) + " MB", chartData);
		}
		
		public Series<String, Number> getThreadSeries(String timeFormat) {
			ObservableList<XYChart.Data<String,Number>> chartData = FXCollections.observableArrayList();
			switch(timeFormat) {
			case "twenty seconds":
				Formatter.formatDataToTwentySeconds(chartData, ThreadData);
				break;
			case "minute":
				Formatter.formatDataToMinute(chartData, ThreadData);
				break;
			case "five minutes":
				Formatter.formatDataToFiveMinutes (chartData, ThreadData);
				break;
			case "ten minutes":
				Formatter.formatDataToTenMinutes(chartData, ThreadData);
				break;
			case "thirty minutes":
				Formatter.formatDataToThirtyMinutes(chartData, ThreadData);
				break;
			case "hour":
				Formatter.formatDataToHour(chartData, ThreadData);
				break;
			}
			return new Series<String, Number>("Live thread count: " + threadCount + "         Peak thread count: " + peakThreadCount, chartData);
		}	
	
		public Series<String, Number> getCPUSeries(String timeFormat) {
			ObservableList<XYChart.Data<String,Number>> chartData = FXCollections.observableArrayList();
			switch(timeFormat) {
			case "twenty seconds":
				Formatter.formatDataToTwentySeconds(chartData, CPUData);
				break;
			case "minute":
				Formatter.formatDataToMinute(chartData, CPUData);
				break;
			case "five minutes":
				Formatter.formatDataToFiveMinutes (chartData, CPUData);
				break;
			case "ten minutes":
				Formatter.formatDataToTenMinutes(chartData, CPUData);
				break;
			case "thirty minutes":
				Formatter.formatDataToThirtyMinutes(chartData, CPUData);
				break;
			case "hour":
				Formatter.formatDataToHour(chartData, CPUData);
				break;
			}
			return new Series<String, Number>("System CPU load: " + df.format(systemCPULoad) + "%", chartData);
		}
	
		public Series<String, Number> getClassSeries(String timeFormat) {
			ObservableList<XYChart.Data<String,Number>> chartData = FXCollections.observableArrayList();
			switch(timeFormat) {
			case "twenty seconds":
				Formatter.formatDataToTwentySeconds(chartData, ClassData);
				break;
			case "minute":
				Formatter.formatDataToMinute(chartData, ClassData);
				break;
			case "five minutes":
				Formatter.formatDataToFiveMinutes (chartData, ClassData);
				break;
			case "ten minutes":
				Formatter.formatDataToTenMinutes(chartData, ClassData);
				break;
			case "thirty minutes":
				Formatter.formatDataToThirtyMinutes(chartData, ClassData);
				break;
			case "hour":
				Formatter.formatDataToHour(chartData, ClassData);
				break;
			}
			return new Series<String, Number>("Loaded class count: " + LoadedClassCount, chartData);
		}
		
		public Series<String, Number> getRequestSeries(String timeFormat) {
			ObservableList<XYChart.Data<String,Number>> chartData = FXCollections.observableArrayList();
			switch(timeFormat) {
			case "twenty seconds":
				Formatter.formatDataToTwentySeconds(chartData, RequestData);
				break;
			case "minute":
				Formatter.formatDataToMinute(chartData, RequestData);
				break;
			case "five minutes":
				Formatter.formatDataToFiveMinutes (chartData, RequestData);
				break;
			case "ten minutes":
				Formatter.formatDataToTenMinutes(chartData, RequestData);
				break;
			case "thirty minutes":
				Formatter.formatDataToThirtyMinutes(chartData, RequestData);
				break;
			case "hour":
				Formatter.formatDataToHour(chartData, RequestData);
				break;
			}
			return new Series<String, Number>("Processed requests: " + requestCount, chartData);
		}
		
		public void start() {
			Thread updateThread = new Thread(() -> {
				while(update) {
					    	if(update) {
					    		try {
									updateHeapInfo();
									updateThreadInfo();
									updateOSInfo();
									updateClassInfo();
									updateRequestInfo();
									updateRuntimeInfo();
									updateCompilationInfo();
								} catch (AttributeNotFoundException | InstanceNotFoundException | MBeanException
										| ReflectionException | IOException e) {
									update = false;
									e.printStackTrace();
									if(CController != null) CController.stopUpdate();
									error.showAndWait();
									GController.switchSceneToMenu();
								}
					    	}					
					try {
						Thread.sleep(2000);
					} catch (InterruptedException e) {
						e.printStackTrace();
					}
				}
			});
			updateThread.start();	
		}


		public void setCController(CondensedGraphsController ccontroller) {
			this.CController = ccontroller;
			
		}


		public void setGController(GraphsController gcontroller) {
			this.GController = gcontroller;
			gcAlert.initOwner(GController.getWindow());
		}


		public void stop() {
			update = false;
		}


		public void gc() {
			memoryProxy.gc();
			gcAlert.showAndWait();
		}
		
		public void findDeadlock() {
			long[] threads = threadProxy.findDeadlockedThreads();
			if(threads != null) AlertBox.display("Deadlocked threads", "Number of deadlocked threads: " + threads.length);
			else AlertBox.display("Deadlocked threads", "No deadlocked threads!");
		}
		
		public void findMonitorDeadlock() {
			long[] threads = threadProxy.findMonitorDeadlockedThreads();
			if(threads != null) AlertBox.display("Monitor deadlocked threads", "Number of monitor deadlocked threads: " + threads.length);
			else AlertBox.display("Monitor deadlocked threads", "No monitor deadlocked threads!");
		}
		
		public void resetPeakThreadCount() {
			threadProxy.resetPeakThreadCount();
			AlertBox.display("Reset successful!", "Peak thread count reset!");
		}

		public void setWindow(Stage window) {
			this.window = window;
		}
}
