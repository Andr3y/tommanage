package management;

import java.text.SimpleDateFormat;
import java.util.Calendar;

import javafx.collections.ObservableList;
import javafx.scene.chart.XYChart;
import javafx.scene.chart.XYChart.Data;

public class Formatter {
	final static SimpleDateFormat SecondsFormat = new SimpleDateFormat("HH:mm:ss");
	final static SimpleDateFormat MinutesFormat = new SimpleDateFormat("HH:mm");
	final static SimpleDateFormat HourFormat = new SimpleDateFormat("HH"); 
	
	static void formatDataToTwentySeconds(ObservableList<Data<String, Number>> chartData, ObservableList<Data<Calendar, Number>> Data) {
		int start = 0;
		if(Data.size()>10) start = Data.size()-11;
		for (int i = start; i < Data.size(); i++) {
				XYChart.Data<String,Number> d = new XYChart.Data<String, Number>(SecondsFormat.format(Data.get(i).getXValue().getTime()), Data.get(i).getYValue());
				chartData.add(d);
			 }
		}
	
	static void formatDataToMinute(ObservableList<Data<String, Number>> chartData, ObservableList<Data<Calendar, Number>> Data) {
		int start = 0;
		String invisible = "\u200e";
		if(Data.size()>30) start = Data.size()-30;
		for (int i = start; i < Data.size(); i++) {
			XYChart.Data<String,Number> d;
			if(i%3==0) {
				d = new XYChart.Data<String, Number>(SecondsFormat.format(Data.get(i).getXValue().getTime())+ invisible, Data.get(i).getYValue());
				chartData.add(d);
			}
			else if(i%6==0){
				invisible += "\u200e";
				d = new XYChart.Data<String, Number>(invisible, Data.get(i).getYValue());
				chartData.add(d);
			}
		}
		}
	
	static void formatDataToFiveMinutes(ObservableList<Data<String, Number>> chartData, ObservableList<Data<Calendar, Number>> Data) {
		String invisible = "\u200e";
		int start = 0;
		if(Data.size()>150) start = Data.size()-150;
		for (int i = start; i < Data.size(); i++) {
				XYChart.Data<String,Number> d;
				if(i%8==0) {
					d = new XYChart.Data<String, Number>(MinutesFormat.format(Data.get(i).getXValue().getTime())+ invisible, Data.get(i).getYValue());
					chartData.add(d);
				}
				else if(i%3==0){
					invisible += "\u200e";
					d = new XYChart.Data<String, Number>(invisible, Data.get(i).getYValue());
					chartData.add(d);
				}
		}
	}
		
	static void formatDataToTenMinutes(ObservableList<Data<String, Number>> chartData, ObservableList<Data<Calendar, Number>> Data) {
		String invisible = "\u200e";
		int start = 0;
		if(Data.size()>300) start = Data.size()-300;
		for (int i = start; i < Data.size(); i++) {
			XYChart.Data<String,Number> d;
			if(i%12==0) {
				d = new XYChart.Data<String, Number>(MinutesFormat.format(Data.get(i).getXValue().getTime())+invisible, Data.get(i).getYValue());
				chartData.add(d);
				}
			else if(i%4==0) {
				invisible += "\u200e";
				d = new XYChart.Data<String, Number>(invisible, Data.get(i).getYValue());
				chartData.add(d);
			} 
		}
	}
	
	static void formatDataToThirtyMinutes(ObservableList<Data<String, Number>> chartData, ObservableList<Data<Calendar, Number>> Data) {
		String invisible = "\u200e";
		int start = 0;
		if(Data.size()>900) start = Data.size()-900;
		for (int i = start; i < Data.size(); i++) {
			XYChart.Data<String,Number> d;
			if(i%25==0) {
				d = new XYChart.Data<String, Number>(MinutesFormat.format(Data.get(i).getXValue().getTime())+invisible, Data.get(i).getYValue());
				chartData.add(d);
				}
			else if(i%8==0) {
				invisible += "\u200e";
				d = new XYChart.Data<String, Number>(invisible, Data.get(i).getYValue());
				chartData.add(d);
			} 
		}
		}
	static void formatDataToHour(ObservableList<Data<String, Number>> chartData, ObservableList<Data<Calendar, Number>> Data) {
		String invisible = "\u200e";
		int start = 0;
		if(Data.size()>1800) start = Data.size()-1800;
		for (int i = start; i < Data.size(); i++) {
			XYChart.Data<String,Number> d;
			if(i%75==0) {
				d = new XYChart.Data<String, Number>(MinutesFormat.format(Data.get(i).getXValue().getTime())+invisible, Data.get(i).getYValue());
				chartData.add(d);
				}
			else if(i%16==0) {
				invisible += "\u200e";
				d = new XYChart.Data<String, Number>(invisible, Data.get(i).getYValue());
				chartData.add(d);
			} 
		}
	}

	}
